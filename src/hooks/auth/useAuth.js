import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import authRequest from "../../action/auth";

function useAuth() {
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const handleLoginByGoogle = useCallback(
    response => {
      return saveAuth(response);
    },
    [dispatch]
  );

  const saveAuth = tokenData => {
    return authRequest.register(tokenData).then(data => {
      localStorage.setItem("token", data.token);
      dispatch({
        type: "SAVE_AUTH",
        payload: {
          user: data.user,
          token: data.token,
          isLoggedIn: true,
          isAdmin: data.user.role === "ADMIN"
        }
      });
    });
  };

  const handleLogout = useCallback(() => {
    localStorage.clear();
    dispatch({
      type: "LOG_OUT"
    });
  }, [dispatch]);

  return {
    auth,
    handleLoginByGoogle,
    handleLogout
  };
}

export default useAuth;
