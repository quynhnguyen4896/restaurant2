import axios from "axios";
import { baseURL } from "../config";
import { GET_ERRORS } from "./types";

export const addList = list => dispatch => {
  return axios
    .post(baseURL + "/lists", list)
    .then(response => {
      dispatch({
        type: "ADD_LIST",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
      return Promise.reject();
    });
};

export const getList = () => dispatch => {
  return axios
    .get(baseURL + "/lists")
    .then(response => {
      dispatch({
        type: "GET_LIST",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
    });
};

export const editList = list => dispatch => {
  return axios
    .post(baseURL + `/lists/${list.id}`, list)
    .then(response => {
      dispatch({
        type: "EDIT_LIST",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
    });
};

export const deleteList = list => dispatch => {
  return axios
    .delete(baseURL + `/lists/${list._id}`)
    .then(response => {
      dispatch({
        type: "DEL_LIST",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
    });
};
