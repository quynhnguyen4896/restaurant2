import axios from "axios";
import { baseURL } from "../config";
import { GET_ERRORS } from "./types";

export const getUsers = () => dispatch => {
  return axios
    .get(baseURL + "/users")
    .then(response => {
      dispatch({
        type: "GET_USERS",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
      return Promise.reject();
    });
};
