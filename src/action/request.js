import axios from "axios";

const debugData = response => {
  return Promise.resolve(response);
};

const debugError = er => {
  return Promise.reject(er.response);
};

const request = ({ noToken = false } = {}) => {
  const axiosApi = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
      Authorization: `JWT ${window.localStorage.getItem("token")}`
    }
  });

  return {
    get(url, params, options = {}) {
      return axiosApi
        .get(url, { params }, options)
        .then(debugData)
        .catch(debugError);
    },
    post(url, data, options = {}) {
      return axiosApi
        .post(url, data, options)
        .then(debugData)
        .catch(debugError);
    },
    put(url, data, options = {}) {
      return axiosApi
        .put(url, data, options)
        .then(debugData)
        .catch(debugError);
    },
    delete(url, data, options = {}) {
      return axiosApi
        .delete(url, { data }, options)
        .then(debugData)
        .catch(debugError);
    }
  };
};

export default request;
