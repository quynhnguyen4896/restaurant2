import request from "./request";
import { baseURL } from "../config";

const register = user =>
  new Promise((resolve, reject) => {
    request()
      .post(baseURL + "/auth/register", user)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(new Error("Something went wrong!"));
      });
  });

export default {
  register
};

export { register };
