import axios from "axios";
import { baseURL } from "../config";
import { GET_ERRORS } from "./types";

export const addSubscribers = list => dispatch => {
  return axios
    .post(baseURL + "/campaigns", list)
    .then(response => {
      dispatch({
        type: "ADD_SUBSCRIBERS",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
      return Promise.reject();
    });
};

export const subscripbeAction = list => dispatch => {
  list.client_id = process.env.REACT_APP_CLIENT_ID;
  list.client_secret = process.env.REACT_APP_CLIENT_SECRET_ID;
  list.redirect_uri = process.env.REACT_APP_REDIRECT_AUTH_URL;
  list.api_key = process.env.REACT_APP_API_KEY;
  return axios
    .post(baseURL + "/campaigns/subscribe", list)
    .then(response => {
      dispatch({
        type: "ADD_SUBSCRIBE_CHANNEL",
        payload: response.data
      });
    })
    .catch(err => {
      console.log("err---->", err);
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
      return Promise.reject();
    });
};

export const getSubscribers = () => dispatch => {
  return axios
    .get(baseURL + "/campaigns")
    .then(response => {
      dispatch({
        type: "GET_SUBSCRIBERS",
        payload: response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: (err.response || { data: null }).data
      });
      return Promise.reject();
    });
};
