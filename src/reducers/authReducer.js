import { getAuthByToken } from "../utils/auth";

const defaultState = {
  user: null,
  token: getAuthByToken(localStorage.getItem("token")),
  isLoggedIn: false,
  isAdmin: false
};

export default function authReducer(state = defaultState, action) {
  switch (action.type) {
    case "SAVE_AUTH": {
      return {
        ...state,
        ...action.payload,
        loading: true
      };
    }

    case "LOG_OUT": {
      return defaultState;
    }

    default:
      return state;
  }
}
