const initialState = {
  campaign: [],
  campaignsubscribe: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "GET_SUBSCRIBERS":
      return { campaign: action.payload };
    case "ADD_SUBSCRIBE_CHANNEL":
      return { ...state, campaignsubscribe: action.payload };
    default:
      return state;
  }
}
