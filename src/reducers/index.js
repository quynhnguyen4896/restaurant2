import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import authReducer from "./authReducer";
import userReducer from "./userReducer";
import listReducer from "./listReducer";
import campaignReducer from "./campaignReducer";

export default combineReducers({
  errors: errorReducer,
  auth: authReducer,
  users: userReducer,
  list: listReducer,
  campaign: campaignReducer
});
