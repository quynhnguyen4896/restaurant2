import { cloneDeep } from "lodash";

const initialState = {
  list: []
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case "ADD_LIST":
      return {
        ...state,
        list: [...state.list, ...[payload]]
      };
    case "GET_LIST":
      return { ...state, list: payload };
    case "EDIT_LIST":
      const index = state.list.findIndex(item => item._id === payload._id);
      state.list.splice(index, 1, payload);
      return {
        ...state,
        list: cloneDeep(state.list)
      };

    case "DEL_LIST_SUCCESS":
      return "success";

    default:
      return state;
  }
}
