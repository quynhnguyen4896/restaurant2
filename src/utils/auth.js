export const parseJwt = token => {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function(c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};

export const getRedirectByAuth = ({ auth, pathname }) => {
  let url = null;
  const { isLoggedIn, isAdmin } = auth;

  if (isLoggedIn) {
    if (isAdmin) {
      if (pathname !== "/dashboard") {
        url = "/dashboard";
      }
    } else {
      if (pathname !== "/subscription") {
        url = "/subscription";
      }
    }
  } else {
    if (pathname !== "/login") {
      url = "/login";
    }
  }
  return url;
};

export const getAuthByToken = token => {
  const initAuth = {
    user: null,
    token: null,
    isLoggedIn: false,
    isAdmin: false
  };

  if (token && token !== "null") {
    const jwtParsed = parseJwt(token);
    if (jwtParsed._id) {
      return {
        user: jwtParsed,
        token,
        isLoggedIn: true,
        isAdmin: jwtParsed.role === "ADMIN"
      };
    } else {
      return initAuth;
    }
  } else {
    return initAuth;
  }
};
