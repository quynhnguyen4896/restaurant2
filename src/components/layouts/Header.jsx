import React from "react";

import useAuth from "../../hooks/auth";

export default function Header() {
  const { handleLogout } = useAuth();

  return (
    <div className="header" style={{ textAlign: "right", padding: 10 }}>
      <div>
        <button onClick={handleLogout} className="logout">
          <span>Logout</span>
        </button>
      </div>
    </div>
  );
}
