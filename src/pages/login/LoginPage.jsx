import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import qs from "qs";

// hooks
import useAuth from "../../hooks/auth";
import { useEffect } from "react";
import { useRef } from "react";
import axios from "axios";

import { Spinner } from "react-bootstrap";
import "./loginpage.css";

function LoginPage(props) {
  console.log("Loginpage props", props);
  const [loading, setLoading] = useState(false);

  const { handleLoginByGoogle, auth } = useAuth();
  const authRef = useRef({});
  authRef.current = auth;

  const history = useHistory();

  const openGoogleLogin = () => {
    console.log("Loginpage openGoogleLogin props", props);
    const baseUrl = props.baseAuthUrl;
    // process.env.REACT_APP_BASE_AUTH_URL;

    // const params = {
    //   client_id: process.env.REACT_APP_CLIENT_ID,
    //   redirect_uri: process.env.REACT_APP_REDIRECT_AUTH_URL,
    //   scope: `profile email ${process.env.REACT_APP_SCOPE_AUTH_URL}`,
    //   state: "try_google_login",
    //   include_granted_scopes: "true",
    //   response_type: "code",
    //   access_type: "offline",
    //   prompt: "consent"
    // };

    const params = {
      client_id: props.appClientID,
      redirect_uri: props.redirectAuthUrl,
      scope: `profile email ${props.scopeAuthUrl}`,
      state: "try_google_login",
      include_granted_scopes: "true",
      response_type: "code",
      access_type: "offline",
      prompt: "consent"
    };

    const query = [];
    for (let p in params) {
      query.push(`${p}=${params[p]}`);
    }

    window.location.href = baseUrl + query.join("&");
  };

  useEffect(() => {
    if (auth.isLoggedIn && auth.isAdmin) {
      setLoading(false);
      history.push("/dashboard");
    } else if (auth.isLoggedIn && !auth.isAdmin) {
      history.push("/subscription");
    } else if (!auth.isLoggedIn && !auth.isAdmin) {
      const {
        location: { search }
      } = history;
      const params = {};
      const regex = /([^&=]+)=([^&]*)/g;

      let m;

      const fragmentString = search.substring(1);

      while ((m = regex.exec(fragmentString))) {
        params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
      }

      if (Object.keys(params).length > 0) {
        if (params["state"] && params["state"] === "try_google_login") {
          setLoading(true);

          axios
            .post(
              props.baseTokenUrl,
              qs.stringify({
                code: params["code"],
                client_id: props.appClientID,
                client_secret: props.clientSecretID,
                redirect_uri: props.redirectAuthUrl,
                grant_type: "authorization_code"
              }),
              {
                headers: {
                  "content-type": "application/x-www-form-urlencoded"
                }
              }
            )
            .then(res => {
              res.data.client_secret = props.clientSecretID;
              res.data.client_id = props.appClientID;
              res.data.redirect_uri = props.redirectAuthUrl;

              handleLoginByGoogle(res.data);
            });
        }
      }
    }
  }, [history, auth, handleLoginByGoogle]);

  return (
    <div className="login-page" style={{ textAlign: "center", margin: 50 }}>
      {auth && !auth.isLoggedIn && (
        <div styling={{ display: "flex" }}>
          {!loading && props && props.userType && (
            <div
              style={{ marginBottom: "10", fontSize: 20, fontWeight: "bold" }}
            >
              For {props.userType}
            </div>
          )}
          {!loading && (
            <div className="google-btn" onClick={() => {openGoogleLogin(props.redirectUrl)}}>
              <div className="google-icon-wrapper">
                <img
                  className="google-icon"
                  src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                  alt="google-login"
                />
              </div>
              <p className="btn-text">
                <b>Sign in with google</b>
              </p>
            </div>
          )}
        </div>
      )}
      {loading && <Spinner animation="grow" />}
    </div>
  );
}

export default LoginPage;
