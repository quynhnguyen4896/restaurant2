import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { isEmpty } from "lodash";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

import { getUsers } from "../../../action/user";

const columns = [
  {
    dataField: "id",
    text: "ID",
    align: "center",
    headerStyle: () => {
      return {
        width: "5%",
        background: "#007bff",
        color: "white",
        textAlign: "center"
      };
    }
  },
  {
    dataField: "name",
    text: "Name",
    headerStyle: () => {
      return { width: "30%", background: "#007bff", color: "white" };
    }
  },
  {
    dataField: "email",
    text: "Email",
    headerStyle: () => {
      return { width: "40%", background: "#007bff", color: "white" };
    }
  },
  {
    dataField: "date",
    text: "Date",
    sort: true,
    headerStyle: () => {
      return { width: "25%", background: "#007bff", color: "white" };
    }
  }
];

export default function UserPage() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const users = useSelector(state => state.users.users);

  let newArray = [];
  if (!isEmpty(users)) {
    users.map((item, index) => {
      item.id = index + 1;
      item.name = item.google_auth.name || "";
      item.date = item.createdAt;
      return newArray.push(item);
    });
  }

  return (
    <div>
      <BootstrapTable
        keyField="id"
        data={newArray}
        columns={columns}
        pagination={paginationFactory({})}
      />
    </div>
  );
}
