import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { chain, map } from "lodash";
import { Row, Col } from "reactstrap";

import { getSubscribers } from "../../../action/campaign";

const CompletedCamp = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getSubscribers());
  }, [dispatch]);

  const campaigns = useSelector(state => state.campaign.campaign);
  const arr =
    campaigns.filter(
      (v, i, a) =>
        a.findIndex(
          t => t.channelID === v.channelID && t.userID === v.userID
        ) === i
    ) || [];
  const subscribe = chain(arr)
    .groupBy("channelID")
    .map(function(items, vvv) {
      return {
        channelID: vvv,
        title: map(items, "title")
      };
    })
    .value();

  return (
    <div>
      {subscribe &&
        subscribe.map((item, index) => {
          return (
            <div
              key={index}
              style={{ border: "1px solid #e2e2e2", width: 600, margin: 10 }}
            >
              <Row>
                <Col md="4">
                  <div className="channel-pannel-title">Channel ID:</div>{" "}
                </Col>
                <Col md="8">
                  <div>{item.channelID}</div>
                </Col>
              </Row>
              <Row>
                <Col md="4">
                  <div className="channel-pannel-title">Channel Title:</div>{" "}
                </Col>
                <Col md="8">
                  <div>{item.title[0]}</div>
                </Col>
              </Row>
              <Row>
                <Col md="4">
                  <div className="channel-pannel-title">New Subscribers:</div>{" "}
                </Col>
                <Col md="8">
                  <div>{item.title.length}</div>
                </Col>
              </Row>
            </div>
          );
        })}
    </div>
  );
};

export default CompletedCamp;
