import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Select from "react-select";
import { isEmpty, find } from "lodash";
import { Input, Label, Button } from "reactstrap";

import { getList } from "../../../action/list";
import { getUsers } from "../../../action/user";
import { addSubscribers, subscripbeAction } from "../../../action/campaign";

import { Spinner } from "react-bootstrap";

import "./style.css";

const Campaign = () => {
  const dispatch = useDispatch();
  const [userList, setUserList] = useState(null);
  const [channelID, setChannelID] = useState("");
  const [channel, setChannel] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    dispatch(getList());
    dispatch(getUsers());
  }, [dispatch]);

  const list = useSelector(state => state.list.list);
  const users = useSelector(state => state.users.users);
  const channelDatas =
    useSelector(state => state.campaign.campaignsubscribe) || {};

  const lists = [];
  if (!isEmpty(list)) {
    list.map((item, index) => {
      item.value = item._id;
      item.label = item.title;
      return lists.push(item);
    });
  }
  const handleSelectChange = (e, name) => {
    setUserList(e.value);
  };

  const handleAutoSubscription = () => {
    if (channelID === "") {
      alert("Enter Youtube Channel ID");
    } else {
      if (userList && users.length > 0) {
        const userLists = find(list, { _id: userList }).list;

        const selectedusers = [];
        if (userLists) {
          userLists.map((list, index) => {
            const subuser = find(users, { _id: list });
            selectedusers.push(subuser);
            return selectedusers;
          });
        }

        if (Array.isArray(selectedusers)) {
          setLoading(true);
          setChannel(false);
          const list = { selectedusers: selectedusers, channelID: channelID };
          dispatch(subscripbeAction(list)).then(res => {
            setLoading(false);
            setChannel(true);
          });
        }
      } else {
        alert("Please check that users are existed");
      }
    }
  };

  if (!isEmpty(channelDatas)) {
    const userLists = find(list, { _id: userList }).list;

    const selectedusers = [];
    if (userLists) {
      userLists.map((list, index) => {
        const subuser = find(users, { _id: list });
        selectedusers.push(subuser);
        return selectedusers;
      });
    }

    const subscribe = [];
    selectedusers.map(item => {
      const subscriber = {
        channelID: channelID,
        title: channelDatas.snippet.title,
        description: channelDatas.snippet.description,
        userID: item._id
      };
      subscribe.push(subscriber);
      return subscribe;
    });
    dispatch(addSubscribers(subscribe));
  }

  return (
    <div className="subscribe-section">
      <div className="subscribe-select">
        <Select
          name="select_userlist"
          defaultValue={lists[0]}
          options={lists}
          onChange={e => handleSelectChange(e, "select_userlist")}
        />
      </div>
      <div className="subscribe-channel">
        <Label for="channelID">Channel ID</Label>
        <Input
          type="text"
          name="channelID"
          id="channelID"
          placeholder="Youtube Channel ID"
          onChange={e => setChannelID(e.target.value)}
        />
      </div>
      <div className="subscribe-button">
        {!loading && <Button onClick={handleAutoSubscription}>START</Button>}
        {loading && <Spinner animation="grow" />}
      </div>
      {channel === true ? (
        <div
          className="alert alert-success alert-custom"
          role="alert"
          style={{ marginTop: 20 }}
        >
          You have Subscribed Successfully.
        </div>
      ) : null}
    </div>
  );
};

export default Campaign;
