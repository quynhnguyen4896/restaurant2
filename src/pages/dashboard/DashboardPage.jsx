import React, { useState } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import PropTypes from "prop-types";

import {
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Toolbar,
  Typography,
  makeStyles,
  useTheme
} from "@material-ui/core/";
import MenuIcon from "@material-ui/icons/Menu";
// import logo from "../../assets/brand.jpeg";

import routes from "./routes";

const drawerWidth = 240;

const Dashboard = props => {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();

  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.logo}>
        {/* <img src={logo} alt="Logo" /> */}
      </div>

      <List>
        {routes.map(route => (
          <Link
            to={route.path}
            key={route.name}
            style={{ textDecoration: "none" }}
          >
            <ListItem button key={route.name}>
              <ListItemText
                primary={route.name}
                className={classes.listItemText}
              />
            </ListItem>
          </Link>
        ))}
      </List>
    </div>
  );

  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />

        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              position="relative"
              container={container}
              variant="temporary"
              anchor={theme.direction === "rtl" ? "right" : "left"}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper
              }}
              ModalProps={{
                keepMounted: true
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>

          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>

        <div style={{width: "100%"}}>
          <AppBar position="relative" className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                edge="start"
                onClick={handleDrawerToggle}
                className={classes.menuButton}
              >
                <MenuIcon />
              </IconButton>

              <Typography variant="h6" noWrap>
                {routes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.toolbar}
                  />
                ))}
              </Typography>
            </Toolbar>
          </AppBar>

          <main className={classes.content}>
            <div className={classes.toolbar} />

            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.main}
              />
            ))}
          </main>
          </div>
      </div>
    </Router>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
  },

  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },

  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: "100%",
    }
  },

  menuButton: {
    marginRight: theme.spacing(2),

    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },

  toolbar: theme.mixins.toolbar,

  drawerPaper: {
    width: drawerWidth,
    background: "linear-gradient(45deg,#1f2428, #343940)"
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },

  logo: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "10px",
    fontSize: "25px",
    color: "white"
  },
  listItemText: {
    color: "white"
  }
}));

Dashboard.propTypes = {
  container: PropTypes.object
};

export default Dashboard;
