import React from "react";
import UserPage from "./users/UserPage";
import ListPage from "./lists/ListPage";
import Campaign from "./campaign/index";

const routes = [
  {
    path: "/app/admin/users",
    exact: true,
    name: "Authenticated Users",
    toolbar: () => <div>Authenticated Users</div>,
    main: () => <UserPage />
  },
  {
    path: "/app/admin/lists",
    exact: true,
    name: "Lists",
    toolbar: () => <div>Lists</div>,
    main: () => <ListPage />
  },
  {
    path: "/app/admin/Campaign",
    exact: true,
    name: "Campign",
    toolbar: () => <div>Campaign</div>,
    main: () => <Campaign />
  }
];

export default routes;
