import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { isEmpty } from "lodash";
import { Button } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

import { getList, deleteList } from "../../../action/list";
import NewListModal from "./NewListModal";

export default function ListPage() {
  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const [activeRow, setActiveRow] = useState(null);

  const toggle = () => {
    setActiveRow(null);
    setModal(!modal);
  };

  const cellAction = (cell, row) => {
    const editRow = row => e => {
      e.preventDefault();
      setActiveRow(row);
      setModal(true);
    };

    const deleteRow = row => e => {
      e.preventDefault();
      dispatch(deleteList(row))
        .then(() => {
          dispatch(getList());
        })
        .catch(err => {});
    };
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center"
        }}
      >
        <Button
          className="btn-edit-list"
          onClick={editRow(row)}
          style={{ background: "#007bff", border: "none" }}
        >
          <EditIcon style={{ fontSize: 15, color: "white" }} />
        </Button>
        <Button
          className="btn-delete-list"
          onClick={deleteRow(row)}
          style={{ background: "red", border: "none" }}
        >
          <DeleteIcon style={{ fontSize: 15 }} />
        </Button>
      </div>
    );
  };

  const columns = [
    {
      dataField: "id",
      text: "ID",
      align: "center",
      headerStyle: () => {
        return {
          width: "40px",
          background: "#007bff",
          color: "white",
          textAlign: "center"
        };
      }
    },
    {
      dataField: "title",
      text: "List Title",
      headerStyle: () => {
        return {
          width: "30%",
          background: "#007bff",
          color: "white"
        };
      }
    },
    {
      dataField: "description",
      text: "Description",
      headerStyle: () => {
        return {
          width: "40%",
          background: "#007bff",
          color: "white"
        };
      }
    },
    {
      dataField: "api",
      text: "Action",
      align: "center",
      headerStyle: () => {
        return {
          width: "100px",
          background: "#007bff",
          textAlign: "center",
          color: "white"
        };
      },
      formatter: cellAction
    }
  ];

  useEffect(() => {
    dispatch(getList());
  }, [dispatch]);

  const lists = useSelector(state => state.list.list);

  let newArray = [];

  if (!isEmpty(lists)) {
    lists.map((item, index) => {
      item.id = index + 1;
      return newArray.push(item);
    });
  }

  return (
    <div>
      <div style={{ textAlign: "right", padding: 10 }}>
        <Button onClick={toggle}>
          <AddIcon />
          Add New List
        </Button>
      </div>
      <div>
        <BootstrapTable
          keyField="_id"
          data={newArray}
          columns={columns}
          pagination={paginationFactory({})}
        />
        <NewListModal isOpen={modal} toggle={toggle} activeList={activeRow} />
      </div>
    </div>
  );
}
