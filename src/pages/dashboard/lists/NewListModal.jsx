import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import classnames from "classnames";

import { isEmpty } from "lodash";
import { getUsers } from "../../../action/user";
import { addList, editList } from "../../../action/list";

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label,
  Input,
  ListGroup,
  ListGroupItem
} from "reactstrap";

const initialFormState = {
  id: null,
  title: "",
  list: []
};

const NewListModal = props => {
  const dispatch = useDispatch();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [checkedItems, setCheckedItems] = useState([]);

  const [lists, setLists] = useState(props.activeList || initialFormState);

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  useEffect(() => {
    setLists(props.activeList || initialFormState);
    if (props.activeList && props.activeList.title)
      setTitle(props.activeList.title);
    else setTitle("");
    if (props.activeList && props.activeList.description)
      setDescription(props.activeList.description);
    else setDescription("");
    if (props.activeList && props.activeList.list.length > 0)
      setCheckedItems(props.activeList.list);
    else setCheckedItems([]);
  }, [props.activeList]);

  const users = useSelector(state => state.users.users);
  const errors = useSelector(state => state.errors);

  const handleChangeChecked = id => () => {
    const itemIndex = checkedItems.findIndex(item => item === id);
    if (itemIndex === -1) {
      setCheckedItems([...checkedItems, id]);
    } else {
      checkedItems.splice(itemIndex, 1);
      setCheckedItems([...checkedItems]);
    }
  };

  const addListSubmit = e => {
    e.preventDefault();
    const list = { title: title, description: description, checkedItems };
    dispatch(addList(list)).then(props.toggle());
  };

  const editListSubmit = e => {
    e.preventDefault();
    const list = {
      id: props.activeList._id,
      title: title,
      description: description,
      checkedItems
    };
    Promise.resolve()
      .then(() => {
        dispatch(editList(list));
      })
      .then(() => {
        props.toggle();
      })
      .catch(err => {});
  };

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle} centered>
      <ModalHeader toggle={props.toggle}>Add New List</ModalHeader>
      <ModalBody>
        <div>
          <Label for="title">New List Title</Label>
          <Input
            type="text"
            name="title"
            id="title"
            placeholder="New List Name"
            className={classnames("form-control form-control-lg", {
              "is-invalid": errors.title
            })}
            onChange={e => setTitle(e.target.value)}
            value={title}
          />
        </div>

        {errors.title && <div className="invalid-feedback">{errors.title}</div>}

        <div style={{ marginTop: 15 }}>
          <Label for="description">Description</Label>
          <Input
            type="textarea"
            rows="5"
            name="description"
            id="description"
            placeholder="Description"
            className={classnames("form-control form-control-lg", {
              "is-invalid": errors.description
            })}
            onChange={e => setDescription(e.target.value)}
            value={description}
          />
        </div>

        {errors.description && (
          <div className="invalid-feedback">{errors.description}</div>
        )}
        {errors.checkedItems && (
          <div className="invalid-feedback">{errors.checkedItems}</div>
        )}
        <div
          style={{ border: "1px solid green", padding: 5, margin: "10px 0px" }}
        >
          <div style={{ padding: 10 }}>UserList</div>
          <div>
            <ListGroup>
              {!isEmpty(users) &&
                users.map((item, index) => (
                  <ListGroupItem
                    key={item._id}
                    onClick={handleChangeChecked(item._id)}
                  >
                    <div style={{ marginLeft: 10 }}>
                      <div>
                        <Input
                          type="checkbox"
                          checked={checkedItems.includes(item._id)}
                          onChange={() => null}
                        />
                      </div>
                      <div>{item.google_auth.name}</div>
                    </div>
                  </ListGroupItem>
                ))}
            </ListGroup>
          </div>
        </div>
      </ModalBody>
      <ModalFooter>
        {props.activeList ? (
          <Button color="primary" onClick={editListSubmit}>
            Edit
          </Button>
        ) : (
          <Button color="primary" onClick={addListSubmit}>
            Add
          </Button>
        )}
        <Button color="secondary" onClick={props.toggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default NewListModal;
