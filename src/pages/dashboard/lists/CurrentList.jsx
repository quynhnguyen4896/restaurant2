import React, { useState } from "react";
import { useSelector } from "react-redux";

import { isEmpty } from "lodash";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import { Row, Col } from "reactstrap";

import "./style.css";

const columns = [
  {
    dataField: "id",
    text: "ID",
    align: "center",
    headerStyle: () => {
      return {
        width: "10%",
        background: "#007bff",
        color: "white",
        textAlign: "center"
      };
    }
  },
  {
    dataField: "title",
    text: "List Title",
    headerStyle: () => {
      return {
        width: "30%",
        background: "#007bff",
        color: "white"
      };
    }
  },
  {
    dataField: "description",
    text: "Description",
    headerStyle: () => {
      return {
        width: "60%",
        background: "#007bff",
        color: "white"
      };
    }
  }
];
const userColumns = [
  {
    dataField: "id",
    text: "ID",
    align: "center",
    headerStyle: () => {
      return {
        width: "10%",
        background: "#007bff",
        color: "white",
        textAlign: "center"
      };
    }
  },
  {
    dataField: "name",
    text: "Name",
    headerStyle: () => {
      return { width: "40%", background: "#007bff", color: "white" };
    }
  },
  {
    dataField: "email",
    text: "Email",
    headerStyle: () => {
      return { width: "50%", background: "#007bff", color: "white" };
    }
  }
];

const CurrentList = () => {
  const list = useSelector(state => state.list.list);
  const lists = [];
  if (!isEmpty(list)) {
    list.map((item, index) => {
      item.id = index + 1;
      return lists.push(item);
    });
  }

  const user = useSelector(state => state.users.users);

  const [selectedUser, setSelectedUser] = useState([]);

  const rowEvents = {
    onClick: (e, row, rowIndex) => {
      e.preventDefault();

      let users = [];

      row.list.map((list, index) => {
        const userlist = user.filter(item => item._id === list);
        users.push(userlist[0]);
        return users;
      });

      const userList = [];

      if (!isEmpty(users)) {
        users.map((item, index) => {
          item.id = index + 1;
          item.name = item.google_auth.name || "";
          item.date = item.createdAt;
          return userList.push(item);
        });
      }
      setSelectedUser(userList);
    }
  };
  return (
    <div>
      {lists && (
        <Row style={{ marginTop: 10 }}>
          <Col md="4">
            <div>Select Row to view details of List</div>
            <BootstrapTable
              keyField="_id"
              data={lists}
              columns={columns}
              rowEvents={rowEvents}
              pagination={paginationFactory({})}
            />
          </Col>
          <Col md="8">
            <div>Authenticated Users</div>
            {selectedUser && (
              <BootstrapTable
                keyField="_id"
                data={selectedUser}
                columns={userColumns}
                // pagination={paginationFactory({})}
              />
            )}
          </Col>
        </Row>
      )}
    </div>
  );
};

export default CurrentList;
