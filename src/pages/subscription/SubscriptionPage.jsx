import React from "react";
import useAuth from "../../hooks/auth";

function SubscriptionPage() {
  const { auth } = useAuth();

  return (
    <div className="subscription-page" style={{ textAlign: "center" }}>
      <p style={{ fontSize: "30px" }}>
        Welcome USER{" "}
        <b>
          {auth.user && auth.user.google_auth && auth.user.google_auth.name}
        </b>
      </p>
    </div>
  );
}

export default SubscriptionPage;
