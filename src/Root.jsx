import React from "react";

import { Switch, Route } from "react-router-dom";

// hooks
import useAuth from "./hooks/auth";

// pages
import { Login, Subscription, Dashboard } from "./pages";

import Header from "./components/layouts/Header";

function Root(props) {
  console.log("Root loginUrl props", props);
  const { auth } = useAuth();

  return (
    <div style={{width: "100%"}} className="root-page">
      {auth.isLoggedIn && !auth.isAdmin && <Header />}
      <div>
        <Switch>
          {!auth.isLoggedIn && !auth.isAdmin && (
            <Route
              path="/login"
              render={() => <Login {...props} userType="USER" />}
            />
          )}
          {!auth.isLoggedIn && !auth.isAdmin && (
            <Route
              path="/admin"
              render={props => <Login {...props} userType="ADMIN" />}
            />
          )}
          {auth.isLoggedIn && !auth.isAdmin && (
            <Route path="/subscription">
              <Subscription />
            </Route>
          )}
          {auth.isLoggedIn && auth.isAdmin && (
            <Route path="/dashboard">
              <Dashboard />
            </Route>
          )}
          <Route path="/">
            <Login {...props} />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default Root;
