import React from "react";
import ReactDOM from "react-dom";
import Root from "./Root";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import * as serviceWorker from "./serviceWorker";

import "bootstrap/dist/css/bootstrap.min.css";

function App(props) {
  return (
    <Provider store={store}>
  <Router>
    <Root {...props}/>
  </Router>
</Provider>
  );
}

// ReactDOM.render(
//   <Provider store={store}>
//     <Router>
//       <Root />
//     </Router>
//   </Provider>,
//   document.getElementById("iframe1")
// );
// serviceWorker.unregister();

export default App;