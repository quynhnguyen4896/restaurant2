To publish to npm registry:

## build the project:

npm run build

## Login to your npm account:
 
npm login

and then Type in your npm account and password

## Publish private:

npm publish --access restricted

## Publish public:

npm publish --access public

## Usage:

import Subcribe from '@testdev0922/restaurant';

## Style

import '@testdev0922/restaurant/build/main.css';

```
<Subcribe
    apiUrl={"https://lotus-back-api.herokuapp.com/api"}
    appClientID={"1013609563170-4sif8e3uckrd58dhls3hnoek63ch8b24.apps.googleusercontent.com"}
    redirectAuthUrl={"http://localhost:3000/login"}
    scopeAuthUrl={"https://www.googleapis.com/auth/youtube.force-ssl"}
    baseTokenUrl={"https://oauth2.googleapis.com/token"}
    apiKey={"AIzaSyBgIzgQMeWM58eQ1qk4WYLuIbgKni5bens"}
    clientSecretID={"zZ8N1JH7SMjZAHNwGU8iNw_E"}
    baseAuthUrl={"https://accounts.google.com/o/oauth2/v2/auth?"}
/>
```

## To change the name of the package (package.json)
  Examples: "name": "@testdev0922/restaurant",


## Update npm package version (package.json)
  Examples: "version": "0.1.28"


## Delete package permanently
  Examples: npm unpublish @testdev0922/restaurant -f